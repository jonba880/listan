#ifndef LIST_H
#define LIST_H
#include <initializer_list>
#include <memory>
namespace List_NS {


template <typename T>
class List {
private:
    class List_Iterator;
public:
    List() = default;
    List(List const &);
    List(List &&) noexcept;
    List(std::initializer_list<T>);

    List & operator=(List const &)&;
    List & operator=(List &&)&;

    void push_front(const T&);
    void push_back(const T&);

    T back() const;
    T & back();
    T front() const;
    T & front();
    T & at(int idx);
    T const & at(int idx) const;

    int size() const;

    void swap(List & other) noexcept;

    List_Iterator begin() const;
    List_Iterator end() const;

private:
    struct Node ;
    std::shared_ptr<Node> head{};
    Node * tail {};
    int sz {};

    class List_Iterator : 
	public std::iterator<std::bidirectional_iterator_tag, T> 
    {
    public:
	List_Iterator(Node* ptr) : node_ptr{ptr}{};

	bool operator!=(const List_Iterator& rhs);
	bool operator==(const List_Iterator& rhs);
	T* operator->();
	T operator*() const;
	List_Iterator& operator++();	
	List_Iterator operator++(int);	 
	List_Iterator& operator--();	
	List_Iterator operator--(int);   
    private:
	Node* node_ptr{nullptr};
    };
};
}
#include "List.tcc"
#endif
