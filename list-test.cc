#include "List.h"
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <string>


using namespace List_NS;

TEST_CASE("Create list")
{
    List<double> lst{1,4,2,6,8,9};
    CHECK(lst.at(2) == 2);
    CHECK(lst.size() == 6);
    List<double> l2;
    l2 = lst;
    CHECK(l2.size() == lst.size());
    CHECK(l2.front() == lst.front());
    CHECK(l2.back() == lst.back());
}

TEST_CASE( "push front/back" )
{
    List<double> lst{1,56,4,6,42};
    lst.push_front(23);
    CHECK(lst.at(0) == 23);
    lst.push_back(345);
    CHECK(lst.at(lst.size()-1) == lst.back());
}

TEST_CASE("Copy constructor")
{
    List<double> lst{1,2,3,4};
    List<double> lst1{lst};
    CHECK(lst.size() == lst1.size());
    CHECK(lst.at(2) == lst1.at(2));
}

TEST_CASE( "default constructor" )
{
    List<double> lst{};
    CHECK(lst.size() == 0);
}

TEST_CASE( "operator =" )
{
    List<double> lst{1,2,3,4};
    List<double> lst1 = lst;
    CHECK(lst.size() == lst1.size());
    CHECK(lst.at(2) == lst1.at(2));
}

TEST_CASE( "operator ==" )
{
    List<double> lst{1,2,3,4};
    CHECK(lst.begin() == lst.begin());
}

TEST_CASE( "operator !=" )
{
    List<double> lst{1,2,3,4};
    CHECK(lst.begin() != lst.end());
}

TEST_CASE( "operator *" )
{
    List<double> lst{7,2,3,4};
    CHECK(*(lst.begin()) == 7);
}

TEST_CASE( "operator ->" )
{
    List<std::string> lst{"aaa","bbb","ccc"};
    std::string str = "";
    for(auto it = lst.begin(); it != lst.end(); ++it){
	str += it->front();
    }
    CHECK(str == "abc");	
}

TEST_CASE( "++ postfix operator" )
{
    List<double> lst{1,2,3,4};
    auto it = lst.begin();
    CHECK(*++it == 2); 
}

TEST_CASE( "++ prefix operator" )
{
    List<double> lst{1,2,3,4};
    auto it = lst.begin();
    CHECK(*it++ == 1); 
    CHECK(*it == 2); 
}

TEST_CASE( "-- postfix operator" )
{
    List<double> lst{1,2,3,4};
    auto it = lst.begin();
    ++it;
    CHECK(*--it == 1); 
}

TEST_CASE( "-- prefix operator" )
{
    List<double> lst{1,2,3,4};
    auto it = lst.begin();
    ++it;
    CHECK(*it-- == 2); 
    CHECK(*it == 1); 
}

TEST_CASE( "iterator" )
{
    List<double> lst{1,2,3,4};
    int temp = 0;
    for(auto it = lst.begin(); it != lst.end(); ++it){
	temp++;
    }
    CHECK(temp == lst.size());	
}



