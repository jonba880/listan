template <typename T>
struct List_NS::List<T>::Node 
{
    T value {};
    Node * prev {};
    std::shared_ptr<Node> next {};
    Node(T v, Node* p = nullptr, std::shared_ptr<Node> n = {})
        : value{v}, prev{p}, next{n}
    {
    }  
};

template <typename T>
List_NS::List<T>::List(List<T> const & other)
    : sz{other.sz}
{
    if (other.size() == 0)
        return;
    Node * tmp {other.head.get()};
    head = std::make_shared<Node>(other.head->value);
    tail = head.get();
    tmp = tmp->next.get();
    while (tmp != nullptr)
    {
        tail->next = std::make_shared<Node>(tmp->value, tail);
        tail = tail->next.get();
        tmp = tmp->next.get();
    }
}

template <typename T>
List_NS::List<T>::List(List<T> && tmp) noexcept {
    swap(tmp);
}

template <typename T>
List_NS::List<T>::List(std::initializer_list<T> lst) : 
head{std::make_shared<Node>(*lst.begin())}, tail{head.get()}, sz{1}
{
    for ( auto it = std::next(lst.begin()); it != std::end(lst); ++it )
    {
        tail->next = std::make_shared<Node>(*it, tail);
        tail = tail->next.get();
        ++sz;
    }
}

template <typename T>
void List_NS::List<T>::push_front(const T& value)
{
    head = std::make_shared<Node>(value, nullptr, head);
    head->next->prev = head.get();
    ++sz;
}
template <typename T>
void List_NS::List<T>::push_back(const T& value)
{
    tail->next = std::make_shared<Node>(value, tail);
    tail = tail->next.get();
    ++sz;
}
template <typename T>
T List_NS::List<T>::back() const
{
    return tail->value;
}
template <typename T>
T & List_NS::List<T>::back()
{
    return tail->value;
}

template <typename T>
typename List_NS::List<T>::List_Iterator List_NS::List<T>::begin() const 
{
    return List_Iterator(head.get());
}

template <typename T>
typename List_NS::List<T>::List_Iterator List_NS::List<T>::end() const 
{
    return List_Iterator(nullptr) ;
}

template <typename T>
T List_NS::List<T>::front() const
{
    return head.get()->value;
}

template <typename T>
T & List_NS::List<T>::front()
{
    return head.get()->value;
}

template <typename T>
T & List_NS::List<T>::at(int idx)
{
    return const_cast<T &>(static_cast<List<T> const &>(*this).at(idx));
}

template <typename T>
T const & List_NS::List<T>::at(int idx) const
{
    if (idx >= sz)
        throw std::out_of_range{"Index not found"};
    Node * tmp {head.get()};
    while ( idx > 0 )
    {
        tmp = tmp->next.get();
        --idx;
    }
    return tmp->value;
}

template <typename T>
int List_NS::List<T>::size() const
{
    return sz;
}

template <typename T>
void List_NS::List<T>::swap(List<T> & other) noexcept
{
    using std::swap;
    swap(head, other.head);
    swap(tail, other.tail);
    swap(sz, other.sz);
}

template <typename T>
List_NS::List<T> & List_NS::List<T>::operator=(List<T> const & rhs) &
{
    List{rhs}.swap(*this);
    return *this;
}

template <typename T>
List_NS::List<T>& List_NS::List<T>::operator=(List<T> && rhs) &
{
    swap(rhs);
    return *this;
}

template <typename T>
bool List_NS::List<T>::List_Iterator::operator!=(const List<T>::List_Iterator& rhs)
{
    return node_ptr != rhs.node_ptr;
}

template <typename T>
bool List_NS::List<T>::List_Iterator::operator==(const List<T>::List_Iterator& rhs) 
{
    return node_ptr == rhs.node_ptr;
}

template <typename T>
typename List_NS::List<T>::List_Iterator& List_NS::List<T>::List_Iterator::operator++()
{
    node_ptr = node_ptr->next.get();
    return *this;
}

template <typename T>
typename List_NS::List<T>::List_Iterator List_NS::List<T>::List_Iterator::operator++(int) 
{
    List_NS::List<T>::List_Iterator temp(*this);
    node_ptr = node_ptr->next.get();
    return temp;
}

template <typename T>
typename List_NS::List<T>::List_Iterator& List_NS::List<T>::List_Iterator::operator--()
{
    node_ptr = node_ptr->prev;
    return *this;
}

template <typename T>
typename List_NS::List<T>::List_Iterator List_NS::List<T>::List_Iterator::operator--(int) 
{
    List_NS::List<T>::List_Iterator temp(*this);
    node_ptr = node_ptr->prev;
    return temp;
}

template <typename T>
T List_NS::List<T>::List_Iterator::operator*() const 
{
    return node_ptr->value;
}

template <typename T>
T* List_NS::List<T>::List_Iterator::operator->() 
{
    return &(node_ptr->value);
}





